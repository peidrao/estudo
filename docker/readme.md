# Docker

## Comandos

> docker pull [nome da imagem]

Baixar imagem

> docker images

Listar imagens

> docker run [nome da imagem]

Iniciar imagem

> docker ps

Listar containers ativos 

> docker exec [id do container] [comando]

Executar comandos do container


## Docker-compose

Postgres + pgadmin4

```
version: '3'

services:
  db:
    image: postgres
    container_name: postgress-container
    restart: always
    environment:
      POSTGRES_PASSWORD: "postgress"
    ports:
      - "15432:5432"
    volumes:
      - home/peidrao/teste/postgress
    networks:
      - postgres-compose-network
      
  teste-pgadmin-compose:
    image: dpage/pgadmin4
    environment:
      PGADMIN_DEFAULT_EMAIL: "peidrao@outlook.com"
      PGADMIN_DEFAULT_PASSWORD: "@postgress"
    ports:
      - "16543:80"
    depends_on:
      - db
    networks:
      - postgres-compose-network

networks: 
  postgres-compose-network:
    driver: bridge
```