# Eslint & Prettier

## Instalação ESLINT

> yarn add eslint

> yarn eslint --init

## Instalação PRETTIER

> yarn add --dev --exact prettier eslint-plugin-prettier

### Configuração

Para que a integração entre o Eslint e o Prettierc aconteça é preciso adicionar no arquivo _.eslintrc.js_ o seguinte código:

```
{
   extends: [
    'airbnb',
    'plugin:prettier/recommended',
  ],
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": "error"
  }
}
```

Depois basta criar um arquivo _.prettierrc_ e adicionar:

```
{
  "singleQuote": true,
  "trailingComma": "es5"
}
```


Caso nenhuma mudança aconteça, pode tentar no _settings.json_ adicionar essa linha:

>  "prettier.requireConfig": true